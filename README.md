gitlab_runner_core
=========
The main ec2 instance will be setup. We will be using the following:

ami = ami-1c81ce7d  (this is a stigged ami)

size = t2.small


This is done to setup the core ci runner in the core vpc

You will need the rpm for the gitlab runner. 

Here is the location of the runner

https://docs.gitlab.com/runner/install/



Requirements
------------
Gitlab Server is setup in the core vpc. Please ensure you have the proper security groups setup if you are going across vpcs. If you are in the same VPC you should not have to worry about that.

Role Variables
--------------



Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------
Tameika Reed
