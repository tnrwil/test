terraform {
backend "s3" {
  bucket = "cie-tfstate"
  key    = "gitlab/gitlab_runner_core/terraform.tfstate"
  region = "us-gov-west-1"
  }
}
